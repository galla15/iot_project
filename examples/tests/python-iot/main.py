from google.cloud import iot_v1

client = iot_v1.DeviceManagerClient()

parent = client.location_path('smartbuilding-297507', 'europe-west1')


# Iterate over all results
for element in client.list_device_registries(parent):
    # process element
    pass

# Or iterate over results one page at a time
for page in client.list_device_registries(parent).pages:
    for element in page:
        # process element
        pass