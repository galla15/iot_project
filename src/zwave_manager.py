from flask.json import jsonify
import zwave
from device import Device
import threading

import sys, os, re, json

class ZwaveManager(Device):

    zwave_backend = None
    sensor_node = 0
    dimmer_node = 0
    nodes = 1
    tag = ""

    def __init__(self):
        super().__init__()

        tag = self.__class__.__name__

        my_name = re.split('\.py', __file__)[0]

        if not my_name:
            raise RuntimeError("my_name not set")

        user_path = os.path.expanduser(
            os.path.expandvars('~/tmp/OZW/'))
        try:
            os.makedirs(user_path, exist_ok=True)
        except Exception as e:
            sys.exit("Can't create user_path: {}".format(e))


        try:
            self.zwave_backend = zwave.Backend_with_dimmers_and_sensors(
                ozw_config_path='/etc/openzwave',
                    ozw_user_path=user_path,
                    log_level=0
            )
            try:
                self.zwave_backend.start()
                print(self.getNetwork())
                self.getNodeList()
                print(self.jsonToStr(self.getNodeList()))
                pass
            except Exception as e:
                sys.exit(e)

        except KeyboardInterrupt:
            self.zwave_backend.stop()
            print("Bye, bye!")


        self.zwave_backend.set_dimmer_level()
    def getNodeList(self):
        """Return a dict of the network nodes
        """
        return self.zwave_backend.get_nodes_list()

    def jsonToStr(self, json_obj):
        """Converts a dict in to a string"""
        return json.dumps(json_obj, indent=2)

    def getNetwork(self):
        net_str = self.zwave_backend.network_info()
        return self.jsonToStr(net_str)

    def interrupt_handler(self, signal, frame):
        self.zwave_backend.stop()
        print("Zwave backend stopped")
        sys.exit(0)

    def sub_callback(self, message):

        msg = message.data.decode('utf-8')
        msg = msg.split(' ')

        dev, payload = msg[0], msg[1:]
        print("[{}] Received :{}".format(self.tag, msg))

        if dev == 'network':
            cmd = payload[0] 

            if cmd == 'add':
                print('[{}]Add node'.format(self.tag))
                self.nodes += 1
                ret = self.zwave_backend.add_node()
                
                if self.zwave_backend._is_sensor(self.nodes):
                    self.sensor_node = self.nodes
                
                elif self.zwave_backend._is_dimmer(self.nodes):
                    self.dimmer_node = self.nodes
                
                else:
                    print("[{}]Unknown device type".format(self.tag))
                    self.nodes -= 1
                    return

                print(self.jsonToStr(ret))
            
            elif cmd == 'remove':
                print('[{}]Remove node'.format(self.tag))
                ret = self.zwave_backend.remove_node()
                print(self.jsonToStr(ret))
            
            elif cmd == 'reset':
                print('[{}]Reset network'.format(self.tag))
                status, msg = self.zwave_backend.hard_reset()
                print("[{}]Result : {}, Data : {}".format(self.tag, status, msg))
            else:
                print('Wrong command')

                self.zwave_backend.s

                self.zwave_backend.set_dimmer_level()
            
        return super().sub_callback(message)

    def publish_sensorTelemetry(self):
        if(self.sensor_node > 0):
            data = jsonify(self.zwave_backend.get_sensor_readings(self.sensor_node))
            print("[{}] Publishing readings : {}".format(self.tag, data))
            self.publish("zwave_data", data)
        
        else:
            print("[{}]No sensor configured".format(self.tag))

