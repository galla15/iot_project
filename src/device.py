from google.cloud import pubsub_v1
import json
from google.auth import jwt


class Device:
    """
    Generic class to publish and subscribe to google cloud Pub/Sub Topics
    """
    client = None
    project_id = 'smartbuilding-297507'
    publisher = None
    subscriber = None
    futures = dict()
    receiving_msg = False

    def __init__(self, project_id=None):
        """
        Constructor
        :param project_id: Google cloud project name
        """
        service_account_info = json.load(open("../authentication/Smartbuilding-2a82889bed47.json"))

        audience = "https://pubsub.googleapis.com/google.pubsub.v1.Subscriber"
        credentials = jwt.Credentials.from_service_account_info(service_account_info, audience=audience)
        self.subscriber = pubsub_v1.SubscriberClient(credentials=credentials)

        audience = "https://pubsub.googleapis.com/google.pubsub.v1.Publisher"
        credentials = jwt.Credentials.from_service_account_info(service_account_info, audience=audience)
        self.publisher = pubsub_v1.PublisherClient(credentials=credentials)

    def get_callback(self, f, data):
        """
        publish callaback
        :param self:
        :param f: futures
        :param data: payload
        :return: func
        """

        def callback(f):
            try:
                self.futures.pop(data)
            except:
                print("Please handle {} for {}.".format(f.exception(), data))

        return callback

    def publish(self, topic, payload=""):
        """
        Publish to a topic
        :param topic: String topic
        :param payload: String Payload
        :return:
        """
        _topic = "projects/{}/topics/{}".format(self.project_id, topic)
        self.futures.update({payload: None})
        future = self.publisher.publish(_topic, payload.encode("utf-8"))
        self.futures[payload] = future
        future.add_done_callback(self.get_callback(future, payload))

    def sub_callback(self, message):
        """
        Subscription callback
        :param message: String the data received
        :return: None
        """
        print("[{}] Sending ack".format(self.__class__.__name__))
        message.ack()
        self.receiving_msg = False

    def subscribe(self, subscription, qos=0):
        """
        Subscribe to a subscription
        :param subscription: String subscription name
        :param qos: Int Not used
        :return: None
        """
        _subscription = "projects/{}/subscriptions/{}".format(self.project_id, subscription)
        print("[{}]Subscribed to : {}".format(self.__class__.__name__, _subscription))
        future = self.subscriber.subscribe(_subscription, self.sub_callback)
