#!/bin/bash

CURRENT=`pwd`
BASENAME=`basename "$CURRENT"`
echo "$BASENAME"

gcloud functions deploy $BASENAME \
--runtime python37 \
--trigger-topic android_knx_commands \
--region europe-west1 