import signal
import time

from knx_device import KNXdevice
from zwave_manager import ZwaveManager


def main():
    # KNX
    knx = KNXdevice()
    knx.subscribe("knx_sub")

    # Zwave
    zwave = ZwaveManager()
    zwave.subscribe("zwave_commands_sub")
    signal.signal(signal.SIGINT, zwave.interrupt_handler)

    while True:
        zwave.publish_sensorTelemetry()
        time.sleep(2)


if __name__ == "__main__":
    main()
