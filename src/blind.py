import knx_obj


class Blind(knx_obj.KNXobj):
    addr = ['0', '0', '0']

    def full_close(self):
        return self.send_cmd("1 1 2")

    def full_open(self):
        return self.send_cmd("0 1 2")

    def get(self):
        return self.send_cmd("0 1 0")

    def set(self, value):
        payload = ""
        payload += str(value)
        payload += " 2 2"
        return self.send_cmd(payload)
