import knx_client


class KNXobj():
    """
    Generic class for a knx object
    """
    addr = ['0', '0', '0']
    gateway_ip = '127.0.0.1'
    gateway_port = '3671'
    control_endpoint = '127.0.0.1:3672'
    data_endpoint = '127.0.0.1:3672'

    def __init__(self, addr):
        """
        Constructor
        :param addr: String device knx address (ex. 4/1)
        """
        if addr:
            addr = addr.replace('\'', '')
            vals = addr.split('/')
            length = len(vals)

            if length < 2:
                SystemError("Address to short")
            elif length == 2:
                self.addr[1] = vals[0]
                self.addr[2] = vals[1]
            elif length == 3:
                self.addr[0] = vals[0]
                self.addr[1] = vals[1]
                self.addr[2] = vals[2]
            else:
                SystemError("Address to long")

    def send_cmd(self, payload):
        """
        Send a command to a knx device
        :param payload: data size apci
        :return: tuple (bool, reason)
        """
        dest_addr = ''

        for i in self.addr:
            dest_addr += i
            dest_addr += '/'

        last = dest_addr.rfind('/')
        dest_addr = dest_addr[:last]

        payload = payload.split(' ')
        payload = int(payload[0]), int(payload[1]), int(payload[2])

        print("[{}]sending command -> addr : {}, payload : {}".format(self.__class__.__name__, dest_addr, payload))

        try:
            reply = knx_client.send_knx_request(
                dest_addr,
                payload,
                self.gateway_ip,
                self.gateway_port,
                self.control_endpoint,
                self.data_endpoint,
            )
        except AttributeError:
            # traceback.print_exc()
            reply = 'Result', 'Error'
            SystemError("Error")

        print('[{}]Result: {}'.format(self.__class__.__name__, reply))

        return reply
