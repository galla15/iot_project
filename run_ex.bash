#!/bin/bash

python3 examples/python-docs-samples/iot/api-client/mqtt_example/cloudiot_mqtt_example.py \
    --registry_id=z-wave-reg \
    --cloud_region=europe-west1 \
    --project_id=smartbuilding-297507 \
    --device_id=dimmer \
    --algorithm=RS256 \
    --private_key_file=key/rsa_private.pem \
    --ca_certs=certificates/roots.pem