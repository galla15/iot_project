import re
from blind import Blind
from device import Device
from valve import Valve
import sys

"""Use the named commands in the mqtt payload
Example:
    valve set '4/1' 50
    valve get '4/1'

    blind open '4/1'
    blind close '4/1'
    blind set '4/1' 50
    blind get '4/1'
"""


class KNXdevice(Device):
    """
    Knx device
    """

    def sub_callback(self, message):
        """
        Subscription callback
        :param message: String data received
        :return: super().subCallback(message)
        """
        data = str(message.data.decode("utf-8"))
        data = data.split(' ')

        print("[{}]Received data : {}".format(self.__class__.__name__, data))

        if len(data) < 2:
            return

        name, payload = data[0], data[1:]

        if not name == 'raw':
            cmd, payload = payload[0], payload[1:]

            if name == 'valve':

                if cmd == 'get':
                    valve = Valve(payload[0])
                    res = valve.get()
                    if res[0]:
                        data = "{\"valve\":" + str(res[1]) + "}"
                        self.publish("knx_data", data)

                if cmd == 'set':
                    addr, value = payload[0], payload[1]
                    valve = Valve(addr)
                    data = int((int(value) / 100) * 255)
                    res = valve.set(value)
                    if res[0]:
                        print("[{}]Result : {}".format(self.__class__.__name__, res[1]))

            elif name == 'blind':
                if cmd == 'open':
                    action = '1/' + payload[0]
                    blind = Blind(action)
                    res = blind.full_open()
                    if res[0]:
                        print("[{}]Result : {}".format(self.__class__.__name__, res[1]))

                if cmd == 'close':
                    action = '1/' + payload[0]
                    blind = Blind(action)
                    res = blind.full_close()
                    if res[0]:
                        print("[{}]Result : {}".format(self.__class__.__name__, res[1]))

                if cmd == 'get':
                    action = '4/' + payload[0]
                    blind = Blind(action)
                    res = blind.get()
                    if res[0]:
                        data = "{\"blind\":" + str(res[1]) + "}"
                        self.publish("knx_data", data)

                if cmd == 'set':
                    addr, value = payload[0], payload[1]
                    data = int((int(value) / 100) * 255)
                    action = '3/' + addr
                    blind = Blind(action)
                    res = blind.set(data)
                    if res[0]:
                        print("[{}]Result : {}".format(self.__class__.__name__, res[1]))
            else:
                print("[{}] Error wrong object : {}".format(self.__class__.__name__, name))
        else:
            print("[{}] Raw format not supported".format(self.__class__.__name__))

        return super().sub_callback(message)
