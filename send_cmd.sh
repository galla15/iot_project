#!/bin/bash

gcloud iot devices commands send \
    --command-data="blind get 4/1" \
    --region=europe-west1  \
    --registry=knx-reg \
    --device=knx-dev 
