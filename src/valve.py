import knx_obj


class Valve(knx_obj.KNXobj):
    addr = ['0', '0', '0']

    def get(self):
        return self.send_cmd("0 1 0")

    def set(self, value):
        payload = ""
        payload += str(value)
        payload += " 2 2"
        return self.send_cmd(payload)
