class Rule:
    """
    Generic rule class
    """
    low = 0.0
    high = 0.0
    actionLower = None
    actionHigher = None
    actionInRange = None
    actionStr = ""
    name = ""
    actionLowerDone = False
    actionHigherDone = False
    actionInRangeDone = False

    def __init__(self, low_val, high_val, name=""):
        """
        Constructor
        :param low_val: Double lower boundary
        :param high_val: Double higher boundary
        :param name: String rule name
        """
        self.low = low_val
        self.high = high_val
        self.name = name
        self.actionStr = "[{}]Rule name : {}, Action : ".format(self.__class__.__name__, self.name)

    def check(self, val):
        """
        Checks a value and apply the corresponding action
        :param val: Double value to check
        :return: None
        """
        if val <= self.low:
            if self.actionLower:
                if not self.actionLowerDone:
                    self.actionLower()
                    print(self.actionStr + "Lower")
                    self.actionLowerDone = True
                    self.actionHigherDone = False
                    self.actionInRangeDone = False

        elif val >= self.high:
            if self.actionHigher:
                if not self.actionHigherDone:
                    self.actionHigher()
                    print(self.actionStr + "Higher")
                    self.actionLowerDone = False
                    self.actionHigherDone = True
                    self.actionInRangeDone = False

        else:
            if self.actionInRange:
                if not self.actionInRangeDone:
                    self.actionInRange()
                    print(self.actionStr + "In range")
                    self.actionLowerDone = False
                    self.actionHigherDone = False
                    self.actionInRangeDone = True


class Rules:
    """
    List of rules
    """
    ruleList = []

    def addRule(self, rule=None):
        """
        Add a new rule to the list
        :param rule: Rule new rule
        :return: None
        """
        if rule:
            self.ruleList.append(rule)

    def checkRules(self, values):
        """
        Check the list and apply the necessary rules
        :param values: List[Tuple(String rule name, Double value)]
        :return: None
        """
        for val in values:
            name, value = val[0], val[1]
            # print(name ,value)

            for r in self.ruleList:
                if name == r.name:
                    r.check(value)
